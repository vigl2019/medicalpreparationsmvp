package com.example.medicalpreparationsmvp.repository;

import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.ResponseModel;
import com.example.medicalpreparationsmvp.model.ResponseModelId;

import java.util.List;
import io.reactivex.Flowable;

public interface IMedicalPreparationsRepository {

    Flowable<ResponseModel> getMedicalPreparationsByPageNumber(String pageNumber);

    Flowable<ResponseModel> getMedicalPreparations();

    /*
        Flowable<ResponseModel> getMedicalPreparations(String format);
    */

    Flowable<ResponseModel> getMedicalPreparationByTitle(String search);

    Flowable<ResponseModelId> getMedicalPreparationById(String id);

    List<MedicalPreparation> getMedicalPreparationsForSuggestions();

    List<MedicalPreparation> getData(int startPosition, int loadSize);

    void deleteDataBase(String dataBaseName);
}