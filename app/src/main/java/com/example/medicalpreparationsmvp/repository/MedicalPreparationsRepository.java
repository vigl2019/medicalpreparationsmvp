package com.example.medicalpreparationsmvp.repository;

import com.example.medicalpreparationsmvp.api.ApiFactory;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.ResponseModel;
import com.example.medicalpreparationsmvp.model.ResponseModelId;
import com.example.medicalpreparationsmvp.utils.App;
import com.example.medicalpreparationsmvp.utils.Helper;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MedicalPreparationsRepository implements IMedicalPreparationsRepository {

    @Override
    public Flowable<ResponseModel> getMedicalPreparationsByPageNumber(String pageNumber) {
        return ApiFactory.getMedicalPreparationsService()
                .getMedicalPreparationsByPageNumber(pageNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Flowable<ResponseModel> getMedicalPreparations() {

        return ApiFactory.getMedicalPreparationsService()
                .getMedicalPreparations()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Flowable<ResponseModel> getMedicalPreparationByTitle(String search) {
        return ApiFactory.getMedicalPreparationsService()
                .getMedicalPreparationByTitle(search)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Flowable<ResponseModelId> getMedicalPreparationById(String id) {
        return ApiFactory.getMedicalPreparationsService()
                .getMedicalPreparationById(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public List<MedicalPreparation> getMedicalPreparationsForSuggestions() {
        return Helper.jsonToArrayList();
    }

    @Override
    public List<MedicalPreparation> getData(int startPosition, int loadSize) {
        return null;
    }

    @Override
    public void deleteDataBase(String dataBaseName) {
        App.getInstance().getContext().deleteDatabase(dataBaseName);
    }
}