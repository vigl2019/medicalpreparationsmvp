package com.example.medicalpreparationsmvp.repository;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

public final class RepositoryProvider {

    private static IMedicalPreparationsRepository repository;

    private RepositoryProvider(){}

    @NonNull
    public static IMedicalPreparationsRepository provideMedicalPreparationsRepository(){
        if(repository == null){
            repository = new MedicalPreparationsRepository();
        }
        return repository;
    }

    public static void setMedicalPreparationsRepository(@NonNull IMedicalPreparationsRepository iMedicalPreparationsRepository){
        repository = iMedicalPreparationsRepository;
    }

    @MainThread
    public static void init(){
        repository = new MedicalPreparationsRepository();
    }
}
