package com.example.medicalpreparationsmvp.api;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.utils.App;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiFactory {

    private static OkHttpClient client;
    private static volatile IMedicalPreparationsService MedicalPreparationsService;
    private static Context context = App.getInstance().getContext();

    private ApiFactory() {
    }

    @NonNull
    public static IMedicalPreparationsService getMedicalPreparationsService() {
        IMedicalPreparationsService iMedicalPreparationsService = MedicalPreparationsService;

        if (iMedicalPreparationsService == null) {
            synchronized (ApiFactory.class) {
                iMedicalPreparationsService = MedicalPreparationsService;
                if (iMedicalPreparationsService == null) {
                    iMedicalPreparationsService = MedicalPreparationsService = buildRetrofit().create(IMedicalPreparationsService.class);
                }
            }
        }
        return iMedicalPreparationsService;
    }

    public static void recreate(){
        client = null;
        client = getClient();
        MedicalPreparationsService = buildRetrofit().create(IMedicalPreparationsService.class);
    }

    @NonNull
    private static Retrofit buildRetrofit() {

        return new Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url))
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient okHttpClient = client;

        if (okHttpClient == null) {
            synchronized (ApiFactory.class) {
                okHttpClient = client;

                if (okHttpClient == null) {
                    okHttpClient = client = buildClient();
                }
            }
        }
        return okHttpClient;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(LoggingInterceptor.create())
                .build();
    }
}
