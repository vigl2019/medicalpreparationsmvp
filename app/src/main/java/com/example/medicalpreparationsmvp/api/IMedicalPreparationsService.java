package com.example.medicalpreparationsmvp.api;

import com.example.medicalpreparationsmvp.model.ResponseModel;
import com.example.medicalpreparationsmvp.model.ResponseModelId;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IMedicalPreparationsService {

    @GET("v1/medicine/")
    Flowable<ResponseModel> getMedicalPreparationsByPageNumber(@Query("page") String page);

    @GET("v1/medicine/")
    Call<ResponseModel> getMedicalPreparationsByPage(@Query("page") String page);

    @GET("v1/medicine/")
    Flowable<ResponseModel> getMedicalPreparations();

    /*
            @GET("v1/medicine/")
            Flowable<MedicalPreparation> getMedicalPreparations(@Query("format") String format);
    */

    @GET("v1/medicine/")
    Flowable<ResponseModel> getMedicalPreparationByTitle(@Query("search") String search);

    @GET("v1/medicine/{id}")
    Flowable<ResponseModelId> getMedicalPreparationById(@Path("id") String id);
}
