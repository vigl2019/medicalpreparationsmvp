package com.example.medicalpreparationsmvp.view;

public interface ILoadingView {
    void showLoading();
    void hideLoading();
}
