package com.example.medicalpreparationsmvp.view;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.contract.IMedicalPreparationContract;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.MedicalPreparationModel;
import com.example.medicalpreparationsmvp.presenter.MedicalPreparationPresenter;
import com.example.medicalpreparationsmvp.repository.MedicalPreparationsRepository;
import com.example.medicalpreparationsmvp.utils.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicalPreparationActivity extends AppCompatActivity implements IMedicalPreparationContract.IView {

    @BindView(R.id.amp_trade_label)
    TextView tradeLabel;

    @BindView(R.id.amp_manufacturer_name)
    TextView manufacturerName;

    @BindView(R.id.amp_packaging_description)
    TextView packagingDescription;

    @BindView(R.id.amp_composition_description)
    TextView compositionDescription;

    @BindView(R.id.amp_composition_inn)
    TextView compositionInn;

    @BindView(R.id.amp_composition_pharm_form)
    TextView compositionPharmForm;

    private IMedicalPreparationContract.IPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_preparation);

        ButterKnife.bind(this);

        presenter = new MedicalPreparationPresenter(this, new MedicalPreparationModel(new MedicalPreparationsRepository()));
        presenter.initPresenter();
    }

    @Override
    public void initView() {
        initActionBar();
        String id = getIntent().getStringExtra(getString(R.string.medical_preparation_id));
        presenter.loadMedicalPreparationById(id);
    }

    @Override
    public void initActionBar() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void loadData(MedicalPreparation medicalPreparation) {
        tradeLabel.setText(medicalPreparation.getTradeLabel());
        manufacturerName.setText(medicalPreparation.getManufacturerName());
        packagingDescription.setText(medicalPreparation.getPackagingDescription());
        compositionDescription.setText(medicalPreparation.getCompositionDescription());
        compositionInn.setText(medicalPreparation.getCompositionInn());
        compositionPharmForm.setText(medicalPreparation.getCompositionPharmForm());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void showError(String message) {
        Helper.createAlertDialog(message, this);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unSubscribe();
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}