package com.example.medicalpreparationsmvp.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.adapter.MedicalPreparationPageKeyedDataSource;
import com.example.medicalpreparationsmvp.adapter.MedicalPreparationsAdapter;
import com.example.medicalpreparationsmvp.adapter.MedicalPreparationsCustomAdapter;
import com.example.medicalpreparationsmvp.contract.IMedicalPreparationsContract;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.MedicalPreparationsModel;
import com.example.medicalpreparationsmvp.presenter.MedicalPreparationsPresenter;
import com.example.medicalpreparationsmvp.repository.MedicalPreparationsRepository;
import com.example.medicalpreparationsmvp.utils.BackGroundThreadExecutor;
import com.example.medicalpreparationsmvp.utils.Helper;

import java.util.List;
import java.util.concurrent.Executor;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IMedicalPreparationsContract.IView {

    @BindView(R.id.am_list_medical_preparations)
    RecyclerView recyclerView;

    @BindString(R.string.medical_preparation_id)
    String medicalPreparationId;

    @BindString(R.string.page_size)
    String pageSize;

    private IMedicalPreparationsContract.IPresenter presenter;
    private MedicalPreparationsAdapter adapter;
//  private MedicalPreparationsSuggestionAdapter suggestionAdapter;
    private MedicalPreparationsCustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        presenter = new MedicalPreparationsPresenter(this, new MedicalPreparationsModel(new MedicalPreparationsRepository()));
        presenter.initPresenter();
    }

    @Override
    public void initView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        presenter.loadMedicalPreparations();
    }

    @Override
    public void loadDataIntoList() {

        MedicalPreparationPageKeyedDataSource dataSource = new MedicalPreparationPageKeyedDataSource();

        PagedList.Config config = new PagedList.Config.Builder()
                .setInitialLoadSizeHint(Integer.parseInt(pageSize))
                .setEnablePlaceholders(false)
                .setPageSize(Integer.parseInt(pageSize))
                .setPrefetchDistance(5)
                .build();

        PagedList<MedicalPreparation> pagedList = new PagedList.Builder<>(dataSource, config)
                .setFetchExecutor(new BackGroundThreadExecutor())
                .setNotifyExecutor(new MainThreadExecutor())
                .build();

        customAdapter = new MedicalPreparationsCustomAdapter();

        customAdapter.setOnMedicalPreparationClickListener(new MedicalPreparationsCustomAdapter.OnMedicalPreparationClickListener() {
            @Override
            public void onMedicalPreparationClick(MedicalPreparation medicalPreparation) {
                Intent intent = new Intent(MainActivity.this, MedicalPreparationActivity.class);
                intent.putExtra(medicalPreparationId, Integer.toString(medicalPreparation.getId()));
                startActivity(intent);
            }
        });

        customAdapter.submitList(pagedList);
    }

    class MainThreadExecutor implements Executor {
        private final Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable command) {
            handler.post(command);
            recyclerView.setAdapter(customAdapter);
        }
    }

    @Override
    public void loadDataIntoList(List<MedicalPreparation> medicalPreparations) {
        adapter = new MedicalPreparationsAdapter(medicalPreparations);

        adapter.setOnMedicalPreparationClickListener(new MedicalPreparationsAdapter.OnMedicalPreparationClickListener() {
            @Override
            public void onMedicalPreparationClick(MedicalPreparation medicalPreparation) {
                Intent intent = new Intent(MainActivity.this, MedicalPreparationActivity.class);
                intent.putExtra(medicalPreparationId, Integer.toString(medicalPreparation.getId()));
                startActivity(intent);
            }
        });

        // Get all medical preparations for search suggestions
//      List<MedicalPreparation> medicalPreparationList = presenter.loadMedicalPreparationsForSuggestions();

//      suggestionAdapter = new MedicalPreparationsSuggestionAdapter(MainActivity.this, medicalPreparationList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search_by_title).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryRefinementEnabled(true);
        searchView.setQueryHint(getApplicationContext().getString(R.string.hint_search));

        final SearchView.SearchAutoComplete searchAutoComplete = searchView.findViewById(androidx.appcompat.R.id.search_src_text);

        searchAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MedicalPreparation medicalPreparation = (MedicalPreparation) adapterView.getItemAtPosition(i);
                String searchString = medicalPreparation.getCompositionInn().trim();
                searchAutoComplete.setText(searchString);
                presenter.loadMedicalPreparationByTitle(searchString);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.loadMedicalPreparationByTitle(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String searchText) {

                if (!searchText.isEmpty()) {
//                    searchAutoComplete.setAdapter(suggestionAdapter);
//                    suggestionAdapter.getFilter().filter(searchText);
                }

                return true;
            }
        });

        return true;
    }

    @Override
    public void showError(String message) {
        Helper.createAlertDialog(message, this);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unSubscribe();
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}