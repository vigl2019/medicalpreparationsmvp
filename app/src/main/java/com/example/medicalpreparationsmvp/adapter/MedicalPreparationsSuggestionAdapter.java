package com.example.medicalpreparationsmvp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;

import java.util.ArrayList;
import java.util.List;

public class MedicalPreparationsSuggestionAdapter extends ArrayAdapter<MedicalPreparation> {

    private List<MedicalPreparation> medicalPreparationListAll;

    public MedicalPreparationsSuggestionAdapter(@NonNull Context context, @NonNull List<MedicalPreparation> medicalPreparationList) {
        super(context, 0, medicalPreparationList);
        medicalPreparationListAll = new ArrayList<>(medicalPreparationList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_suggestion_item, parent, false);
        }

        TextView suggestionText = convertView.findViewById(R.id.ssi_search_suggestion_text);

        MedicalPreparation medicalPreparation = getItem(position);

        if (medicalPreparation != null) {
            suggestionText.setText(medicalPreparation.getCompositionInn());
        }

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return medicalPreparationFilter;
    }

    private Filter medicalPreparationFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults filterResults = new FilterResults();
            List<MedicalPreparation> suggestions = new ArrayList<>();

            if ((charSequence == null) || (charSequence.length() == 0)) {
                suggestions.addAll(medicalPreparationListAll);
            } else {
                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (MedicalPreparation medicalPreparation : medicalPreparationListAll) {
//                  if (medicalPreparation.getCompositionInn().toLowerCase().contains(filterPattern)) {
                    if (medicalPreparation.getCompositionInn().toLowerCase().startsWith(filterPattern)) {
                        suggestions.add(medicalPreparation);
                    }
                }
            }

            filterResults.values = suggestions;
            filterResults.count = suggestions.size();

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            clear();
            addAll((List) filterResults.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            MedicalPreparation medicalPreparation = (MedicalPreparation) resultValue;
            return medicalPreparation.getCompositionInn();
        }
    };
}