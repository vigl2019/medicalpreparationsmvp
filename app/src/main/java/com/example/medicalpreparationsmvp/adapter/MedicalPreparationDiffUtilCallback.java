package com.example.medicalpreparationsmvp.adapter;

import androidx.recyclerview.widget.DiffUtil;

import com.example.medicalpreparationsmvp.model.MedicalPreparation;

import java.util.List;

public class MedicalPreparationDiffUtilCallback extends DiffUtil.Callback {

    private List<MedicalPreparation> oldList;
    private List<MedicalPreparation> newList;

    public MedicalPreparationDiffUtilCallback(List<MedicalPreparation> oldList, List<MedicalPreparation> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        MedicalPreparation oldMedicalPreparation = oldList.get(oldItemPosition);
        MedicalPreparation newMedicalPreparation = newList.get(newItemPosition);

        return oldMedicalPreparation.getId() == newMedicalPreparation.getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        MedicalPreparation oldMedicalPreparation = oldList.get(oldItemPosition);
        MedicalPreparation newMedicalPreparation = newList.get(newItemPosition);

        return oldMedicalPreparation.getTradeLabel().equals(newMedicalPreparation.getTradeLabel())
                && oldMedicalPreparation.getManufacturerName().equals(newMedicalPreparation.getManufacturerName())
                && oldMedicalPreparation.getPackagingDescription().equals(newMedicalPreparation.getPackagingDescription())
                && oldMedicalPreparation.getCompositionDescription().equals(newMedicalPreparation.getCompositionDescription())
                && oldMedicalPreparation.getCompositionInn().equals(newMedicalPreparation.getCompositionInn())
                && oldMedicalPreparation.getCompositionPharmForm().equals(newMedicalPreparation.getCompositionPharmForm());
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }
}
