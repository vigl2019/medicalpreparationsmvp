package com.example.medicalpreparationsmvp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicalPreparationsCustomAdapter extends PagedListAdapter<MedicalPreparation, MedicalPreparationsCustomAdapter.MedicalPreparationViewHolder> {

    private OnMedicalPreparationClickListener onMedicalPreparationClickListener;

    public interface OnMedicalPreparationClickListener {
        void onMedicalPreparationClick(MedicalPreparation medicalPreparation);
    }

    public void setOnMedicalPreparationClickListener(OnMedicalPreparationClickListener onMedicalPreparationClickListener) {
        this.onMedicalPreparationClickListener = onMedicalPreparationClickListener;
    }

    public MedicalPreparationsCustomAdapter() {
        super(diffUtilCallBack);
    }

    private static DiffUtil.ItemCallback<MedicalPreparation> diffUtilCallBack =
            new DiffUtil.ItemCallback<MedicalPreparation>() {
                @Override
                public boolean areItemsTheSame(@NonNull MedicalPreparation oldMedicalPreparation, @NonNull MedicalPreparation newMedicalPreparation) {
                    return oldMedicalPreparation.getId() == newMedicalPreparation.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull MedicalPreparation oldMedicalPreparation, @NonNull MedicalPreparation newMedicalPreparation) {
                    return oldMedicalPreparation.getTradeLabel().equals(newMedicalPreparation.getTradeLabel())
                            && oldMedicalPreparation.getManufacturerName().equals(newMedicalPreparation.getManufacturerName())
                            && oldMedicalPreparation.getPackagingDescription().equals(newMedicalPreparation.getPackagingDescription())
                            && oldMedicalPreparation.getCompositionDescription().equals(newMedicalPreparation.getCompositionDescription())
                            && oldMedicalPreparation.getCompositionInn().equals(newMedicalPreparation.getCompositionInn())
                            && oldMedicalPreparation.getCompositionPharmForm().equals(newMedicalPreparation.getCompositionPharmForm());
                }
            };

    @NonNull
    @Override
    public MedicalPreparationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medical_preparation_item, parent, false);
        MedicalPreparationViewHolder holder = new MedicalPreparationViewHolder(cardView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MedicalPreparationViewHolder holder, int position) {

        MedicalPreparation medicalPreparation = getItem(position);

        if(medicalPreparation != null){
            holder.bind(medicalPreparation);
        }
    }

    public class MedicalPreparationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mpi_trade_label)
        TextView tradeLabel;

        @BindView(R.id.mpi_manufacturer_name)
        TextView manufacturerName;

        CardView cardView;

        public MedicalPreparationViewHolder(@NonNull CardView itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            cardView = itemView;
        }

        public void bind(final MedicalPreparation medicalPreparation) {
            tradeLabel.setText(medicalPreparation.getTradeLabel());
            manufacturerName.setText(medicalPreparation.getManufacturerName());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMedicalPreparationClickListener != null) {
                        onMedicalPreparationClickListener.onMedicalPreparationClick(medicalPreparation);
                    }
                }
            });
        }
    }
}
