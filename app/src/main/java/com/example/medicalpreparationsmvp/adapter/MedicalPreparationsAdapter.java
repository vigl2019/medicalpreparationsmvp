package com.example.medicalpreparationsmvp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicalPreparationsAdapter extends RecyclerView.Adapter<MedicalPreparationsAdapter.MedicalPreparationsViewHolder> {

    private List<MedicalPreparation> medicalPreparations;
    private OnMedicalPreparationClickListener onMedicalPreparationClickListener;

    public MedicalPreparationsAdapter(List<MedicalPreparation> medicalPreparations) {
        this.medicalPreparations = medicalPreparations;
    }

    public interface OnMedicalPreparationClickListener {
        void onMedicalPreparationClick(MedicalPreparation medicalPreparation);
    }

    public void setOnMedicalPreparationClickListener(OnMedicalPreparationClickListener onMedicalPreparationClickListener) {
        this.onMedicalPreparationClickListener = onMedicalPreparationClickListener;
    }

    @NonNull
    @Override
    public MedicalPreparationsAdapter.MedicalPreparationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medical_preparation_item, parent, false);

        return new MedicalPreparationsAdapter.MedicalPreparationsViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicalPreparationsAdapter.MedicalPreparationsViewHolder holder, int position) {

        MedicalPreparation medicalPreparation = medicalPreparations.get(position);

        holder.tradeLabel.setText(medicalPreparation.getTradeLabel());
        holder.manufacturerName.setText(medicalPreparation.getManufacturerName());
    }

    @Override
    public int getItemCount() {
        return medicalPreparations.size();
    }

    class MedicalPreparationsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mpi_trade_label)
        TextView tradeLabel;

        @BindView(R.id.mpi_manufacturer_name)
        TextView manufacturerName;

        CardView cardView;

        public MedicalPreparationsViewHolder(@NonNull CardView itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            cardView = itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMedicalPreparationClickListener != null) {
                        onMedicalPreparationClickListener.onMedicalPreparationClick(medicalPreparations.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}