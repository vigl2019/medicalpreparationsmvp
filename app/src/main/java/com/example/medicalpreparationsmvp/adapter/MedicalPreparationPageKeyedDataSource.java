package com.example.medicalpreparationsmvp.adapter;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.api.ApiFactory;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.ResponseModel;
import com.example.medicalpreparationsmvp.presenter.MapperMedicalPreparations;
import com.example.medicalpreparationsmvp.utils.App;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicalPreparationPageKeyedDataSource extends PageKeyedDataSource<String, MedicalPreparation> {

    private static final int PAGE_START = Integer.parseInt(App.getInstance().getContext().getString(R.string.page_start));

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull final LoadInitialCallback<String, MedicalPreparation> callback) {

        ApiFactory.getMedicalPreparationsService().getMedicalPreparationsByPage(String.valueOf(PAGE_START))
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        if (response.body() != null) {

                            List<MedicalPreparation> medicalPreparationList = new ArrayList<>();
                            ResponseModel responseModel = response.body();

                            if (responseModel.getResults() != null) {
                                MapperMedicalPreparations mapperMedicalPreparations = new MapperMedicalPreparations();

                                for (int i = 0; i < responseModel.getResults().size(); i++) {
                                    MedicalPreparation medicalPreparation = mapperMedicalPreparations.map(responseModel.getResults().get(i));
                                    medicalPreparationList.add(medicalPreparation);
                                }
                            }
                            callback.onResult(medicalPreparationList, null, String.valueOf(PAGE_START + 1));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
//                      Log.e("loadInitial", t.getMessage());
                        callback.onResult(Collections.<MedicalPreparation>emptyList(), null, null);
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<String> params, @NonNull final LoadCallback<String, MedicalPreparation> callback) {
        ApiFactory.getMedicalPreparationsService().getMedicalPreparationsByPage(params.key)
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                        if (response.body() != null) {
                            List<MedicalPreparation> medicalPreparationList = new ArrayList<>();
                            ResponseModel responseModel = response.body();

                            int paramsKey = Integer.parseInt(params.key);
                            //  int previousPage = (paramsKey > 1) ? (paramsKey - 1) : null;
                            int previousPage = !responseModel.getPrevious().contains("null") ? (paramsKey - 1) : null;

                            if (responseModel.getResults() != null) {
                                MapperMedicalPreparations mapperMedicalPreparations = new MapperMedicalPreparations();

                                for (int i = 0; i < responseModel.getResults().size(); i++) {
                                    MedicalPreparation medicalPreparation = mapperMedicalPreparations.map(responseModel.getResults().get(i));
                                    medicalPreparationList.add(medicalPreparation);
                                }
                            }
                            callback.onResult(medicalPreparationList, String.valueOf(previousPage));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
//                      Log.e("loadBefore", t.getMessage());
                        callback.onResult(Collections.<MedicalPreparation>emptyList(), null);
                    }
                });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<String> params, @NonNull final LoadCallback<String, MedicalPreparation> callback) {
        ApiFactory.getMedicalPreparationsService().getMedicalPreparationsByPage(params.key)
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                        if (response.body() != null) {
                            List<MedicalPreparation> medicalPreparationList = new ArrayList<>();
                            ResponseModel responseModel = response.body();

                            int paramsKey = Integer.parseInt(params.key);
                            int nextPage = !responseModel.getNext().contains("null") ? (paramsKey + 1) : null;

                            if (responseModel.getResults() != null) {
                                MapperMedicalPreparations mapperMedicalPreparations = new MapperMedicalPreparations();

                                for (int i = 0; i < responseModel.getResults().size(); i++) {
                                    MedicalPreparation medicalPreparation = mapperMedicalPreparations.map(responseModel.getResults().get(i));
                                    medicalPreparationList.add(medicalPreparation);
                                }
                            }
                            callback.onResult(medicalPreparationList, String.valueOf(nextPage));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
//                      Log.e("loadAfter", t.getMessage());
                        callback.onResult(Collections.<MedicalPreparation>emptyList(), null);
                    }
                });
    }
}