package com.example.medicalpreparationsmvp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public final class Helper {

    private static final String BASE_URL = App.getInstance().getString(R.string.base_url);
    private static Context context = App.getInstance().getContext();
    private static SharedPreferences sharedPreferences;

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String convertIntToString(int value) {
        return String.valueOf(value);
    }

    public static String getMedicalPreparationTitle(String queryString) {
        String[] query = queryString.split("\n");
        return query[0].trim();
    }

    public static void arrayListToJson(List<MedicalPreparation> medicalPreparationList) {
        Gson gson = new Gson();

        String jsonMedicalPreparationList = gson.toJson(medicalPreparationList);
        String keyString = context.getString(R.string.json_medical_preparation_list);

        sharedPreferences = context.getSharedPreferences(keyString, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(keyString, jsonMedicalPreparationList);
        editor.apply();
    }

    public static List<MedicalPreparation> jsonToArrayList() {
        List<MedicalPreparation> medicalPreparationList = new ArrayList<>();

        sharedPreferences = context.getSharedPreferences(context.getString(R.string.json_medical_preparation_list), Context.MODE_PRIVATE);
        String strMedicalPreparationList = sharedPreferences.getString(context.getString(R.string.json_medical_preparation_list), "");
        Gson gson = new Gson();

        if (!strMedicalPreparationList.isEmpty() || strMedicalPreparationList != null) {
            medicalPreparationList = gson.fromJson(strMedicalPreparationList,
                    new TypeToken<ArrayList<MedicalPreparation>>() {
                    }.getType());
        }
        return medicalPreparationList;
    }

    public static List<MedicalPreparation> jsonToArrayList(int pageIndex, int pageSize) {
        List<MedicalPreparation> medicalPreparationList = jsonToArrayList();
        List<MedicalPreparation> medicalPreparations = new ArrayList<>();

        for (int i = --pageIndex * pageSize; i < pageIndex * pageSize; i++) {
            medicalPreparations.add(medicalPreparationList.get(i));
        }

        return medicalPreparations;
    }

    public static int getTotalPage(int pageSize) {
        List<MedicalPreparation> medicalPreparationList = jsonToArrayList();
        int totalPage = medicalPreparationList.size() / pageSize;

        if (medicalPreparationList.size() % pageSize > 0) {
            totalPage++;
        }
        return totalPage;
    }

    public static void createAlertDialog(String errorMassage, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}


