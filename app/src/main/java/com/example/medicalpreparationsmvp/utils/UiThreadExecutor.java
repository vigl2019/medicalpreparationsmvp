package com.example.medicalpreparationsmvp.utils;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;

public final class UiThreadExecutor implements Executor {

    private Handler handler = new Handler(Looper.getMainLooper());

    @Override
    public void execute(Runnable command) {
        handler.post(command);
    }
}