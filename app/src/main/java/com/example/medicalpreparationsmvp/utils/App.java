package com.example.medicalpreparationsmvp.utils;

import android.app.Application;
import android.content.Context;
import androidx.room.Room;

import com.example.medicalpreparationsmvp.database.MedicalPreparationsDataBase;

public final class App extends Application {

    private static App instance;
    private Context context;
    private MedicalPreparationsDataBase dataBase;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        context = App.this;
        context.deleteDatabase("MedicalPreparationsDataBase");
        dataBase = Room.databaseBuilder(this, MedicalPreparationsDataBase.class, "MedicalPreparationsDataBase")
                .allowMainThreadQueries()
                .build();
    }

    public static App getInstance() {
        return instance;
    }

    public Context getContext() {
        return context;
    }

    public MedicalPreparationsDataBase getDataBase() {
        return dataBase;
    }
}