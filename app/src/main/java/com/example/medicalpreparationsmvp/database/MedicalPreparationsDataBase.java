package com.example.medicalpreparationsmvp.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;

@Database(entities = MedicalPreparation.class, version = 1)
public abstract class MedicalPreparationsDataBase extends RoomDatabase {
    public abstract MedicalPreparationDAO getMedicalPreparationDAO();
}