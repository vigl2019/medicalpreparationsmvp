package com.example.medicalpreparationsmvp.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.medicalpreparationsmvp.model.MedicalPreparation;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class MedicalPreparationDAO {

    @Insert
    public abstract void insertMedicalPreparations(List<MedicalPreparation> medicalPreparations);

    @Query("SELECT * FROM MedicalPreparation")
    public abstract Flowable<List<MedicalPreparation>> getMedicalPreparations();

    @Query("SELECT * FROM MedicalPreparation")
    public abstract List<MedicalPreparation> getMedicalPreparationsForSuggestions();

    @Query("DELETE FROM MedicalPreparation")
    public abstract void removeMedicalPreparations();

    public void updateMedicalPreparations(List<MedicalPreparation> medicalPreparations) {
        removeMedicalPreparations();
        insertMedicalPreparations(medicalPreparations);
    }
}