package com.example.medicalpreparationsmvp.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class MedicalPreparation {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String tradeLabel;
    private String manufacturerName;
    private String packagingDescription;
    private String compositionDescription;
    private String compositionInn;
    private String compositionPharmForm;

//  public MedicalPreparation(){}

    public MedicalPreparation(int id, String tradeLabel, String manufacturerName, String packagingDescription, String compositionDescription, String compositionInn, String compositionPharmForm) {
        this.id = id;
        this.tradeLabel = tradeLabel;
        this.manufacturerName = manufacturerName;
        this.packagingDescription = packagingDescription;
        this.compositionDescription = compositionDescription;
        this.compositionInn = compositionInn;
        this.compositionPharmForm = compositionPharmForm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTradeLabel() {
        return tradeLabel;
    }

    public void setTradeLabel(String tradeLabel) {
        this.tradeLabel = tradeLabel;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getPackagingDescription() {
        return packagingDescription;
    }

    public void setPackagingDescription(String packagingDescription) {
        this.packagingDescription = packagingDescription;
    }

    public String getCompositionDescription() {
        return compositionDescription;
    }

    public void setCompositionDescription(String compositionDescription) {
        this.compositionDescription = compositionDescription;
    }

    public String getCompositionInn() {
        return compositionInn;
    }

    public void setCompositionInn(String compositionInn) {
        this.compositionInn = compositionInn;
    }

    public String getCompositionPharmForm() {
        return compositionPharmForm;
    }

    public void setCompositionPharmForm(String compositionPharmForm) {
        this.compositionPharmForm = compositionPharmForm;
    }
}
