package com.example.medicalpreparationsmvp.model;

import com.example.medicalpreparationsmvp.contract.IMedicalPreparationContract;
import com.example.medicalpreparationsmvp.repository.IMedicalPreparationsRepository;

import io.reactivex.Flowable;

public class MedicalPreparationModel implements IMedicalPreparationContract.IModel {

    private IMedicalPreparationsRepository medicalPreparationsRepository;

    public MedicalPreparationModel(IMedicalPreparationsRepository medicalPreparationsRepository) {
        this.medicalPreparationsRepository = medicalPreparationsRepository;
    }

    @Override
    public Flowable<ResponseModelId> getMedicalPreparationById(String id) {
        return medicalPreparationsRepository.getMedicalPreparationById(id);
    }
}