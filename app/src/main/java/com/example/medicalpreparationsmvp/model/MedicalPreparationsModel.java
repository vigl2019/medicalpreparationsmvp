package com.example.medicalpreparationsmvp.model;

import com.example.medicalpreparationsmvp.contract.IMedicalPreparationsContract;
import com.example.medicalpreparationsmvp.repository.IMedicalPreparationsRepository;

import java.util.List;

import io.reactivex.Flowable;

public class MedicalPreparationsModel implements IMedicalPreparationsContract.IModel {

    private IMedicalPreparationsRepository medicalPreparationsRepository;

    public MedicalPreparationsModel(IMedicalPreparationsRepository medicalPreparationsRepository) {
        this.medicalPreparationsRepository = medicalPreparationsRepository;
    }

    @Override
    public void deleteDataBase(String dataBaseName) {
        medicalPreparationsRepository.deleteDataBase(dataBaseName);
    }

    @Override
    public Flowable<ResponseModel> getMedicalPreparations() {
        return medicalPreparationsRepository.getMedicalPreparations();
    }

    @Override
    public Flowable<ResponseModel> getMedicalPreparationsByPageNumber(String pageNumber) {
        return medicalPreparationsRepository.getMedicalPreparationsByPageNumber(pageNumber);
    }

    @Override
    public Flowable<ResponseModel> getMedicalPreparationByTitle(String title) {
        return medicalPreparationsRepository.getMedicalPreparationByTitle(title);
    }

    @Override
    public List<MedicalPreparation> getMedicalPreparationsForSuggestions() {
        return medicalPreparationsRepository.getMedicalPreparationsForSuggestions();
    }
}
