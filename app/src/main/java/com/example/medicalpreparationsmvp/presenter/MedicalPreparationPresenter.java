package com.example.medicalpreparationsmvp.presenter;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.contract.IMedicalPreparationContract;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.ResponseModelId;
import com.example.medicalpreparationsmvp.utils.App;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class MedicalPreparationPresenter implements IMedicalPreparationContract.IPresenter {

    private IMedicalPreparationContract.IView view;
    private IMedicalPreparationContract.IModel model;
    private CompositeDisposable disposables = new CompositeDisposable();

    public MedicalPreparationPresenter(IMedicalPreparationContract.IView view, IMedicalPreparationContract.IModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void initPresenter() {
        view.initView();
    }

    @Override
    public void loadMedicalPreparationById(String id) {
        Flowable<ResponseModelId> medicalPreparation = model.getMedicalPreparationById(id);
        getMedicalPreparation(medicalPreparation);
    }

    private void getMedicalPreparation(Flowable<ResponseModelId> medicalPreparation) {
        medicalPreparation.map(new Function<ResponseModelId, MedicalPreparation>() {
            @Override
            public MedicalPreparation apply(ResponseModelId responseModelId) {

                MedicalPreparation medicalPreparation = null;

                if (responseModelId != null) {
                    MapperMedicalPreparation mapperMedicalPreparation = new MapperMedicalPreparation();
                    medicalPreparation = mapperMedicalPreparation.map(responseModelId);
                }

                return medicalPreparation;
            }
        })

                .filter(new Predicate<MedicalPreparation>() {
                    @Override
                    public boolean test(MedicalPreparation medicalPreparation) throws Exception {

                        boolean isMedicalPreparationNull = (medicalPreparation != null);

                        if (!isMedicalPreparationNull) {
//                            Log.i(App.getInstance().getString(R.string.TAG_get_Preparation_By_Id),
//                                    App.getInstance().getString(R.string.med_preparation_is_null));
                            view.showError(App.getInstance().getString(R.string.med_preparation_is_null));
                        }

                        return isMedicalPreparationNull;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<MedicalPreparation>() {
                    @Override
                    public void accept(MedicalPreparation medicalPreparation) throws Exception {
                        view.loadData(medicalPreparation);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
//                        Log.e(App.getInstance().getString(R.string.TAG_get_Preparation_By_Id),
//                                throwable.getMessage(), throwable);
                        view.showError(App.getInstance().getString(R.string.something_went_wrong));
                    }
                });
    }

    @Override
    public void unSubscribe() {
        disposables.clear();
    }

    @Override
    public void detachView() {
        view = null;
    }
}