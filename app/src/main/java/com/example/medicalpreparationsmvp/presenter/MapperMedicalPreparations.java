package com.example.medicalpreparationsmvp.presenter;

import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.Result;

public class MapperMedicalPreparations implements IMapper<Result, MedicalPreparation> {
    @Override
    public MedicalPreparation map(Result result) {

        int id = -1;
        if (result.getId() != null) {
            id = result.getId();
        }

        String tradeLabel = "";
        if (result.getTradeLabel() != null) {
            tradeLabel = result.getTradeLabel().getName();
        }

        String manufacturerName = "";
        if (result.getManufacturer() != null) {
            manufacturerName = result.getManufacturer().getName();
        }

        String packagingDescription = "";
        if (result.getPackaging() != null) {
            packagingDescription = result.getPackaging().getDescription();
        }

        String compositionDescription = "";
        if (result.getComposition() != null) {
            compositionDescription = result.getComposition().getDescription();
        }

        String compositionInn = "";
        if (result.getComposition() != null || result.getComposition().getInn() != null) {
            compositionInn = result.getComposition().getInn().getName();
        }

        String compositionPharmForm = "";
        if (result.getComposition() != null || result.getComposition().getPharmForm() != null) {
            compositionPharmForm = result.getComposition().getPharmForm().getName();
        }

        MedicalPreparation medicalPreparation = new MedicalPreparation(id, tradeLabel, manufacturerName,
                packagingDescription, compositionDescription, compositionInn, compositionPharmForm);

        return medicalPreparation;
    }
}
