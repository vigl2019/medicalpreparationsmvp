package com.example.medicalpreparationsmvp.presenter;

import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.ResponseModelId;

public class MapperMedicalPreparation implements IMapper<ResponseModelId, MedicalPreparation> {

    @Override
    public MedicalPreparation map(ResponseModelId responseModelId) {
        int id = responseModelId.getId();
        String tradeLabel = responseModelId.getTradeLabel().getName();
        String manufacturerName = responseModelId.getManufacturer().getName();
        String packagingDescription = responseModelId.getPackaging().getDescription();
        String compositionDescription = responseModelId.getComposition().getDescription();
        String compositionInn = responseModelId.getComposition().getInn().getName();
        String compositionPharmForm = responseModelId.getComposition().getPharmForm().getName();

        MedicalPreparation medicalPreparation = new MedicalPreparation(id, tradeLabel, manufacturerName,
                packagingDescription, compositionDescription, compositionInn, compositionPharmForm);

        return medicalPreparation;
    }
}

