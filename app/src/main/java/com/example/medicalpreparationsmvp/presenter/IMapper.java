package com.example.medicalpreparationsmvp.presenter;

public interface IMapper<From, To> {
    To map(From from);
}