package com.example.medicalpreparationsmvp.presenter;

import com.example.medicalpreparationsmvp.R;
import com.example.medicalpreparationsmvp.contract.IMedicalPreparationsContract;
import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.ResponseModel;
import com.example.medicalpreparationsmvp.utils.App;
import com.example.medicalpreparationsmvp.utils.Helper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class MedicalPreparationsPresenter implements IMedicalPreparationsContract.IPresenter {

    private IMedicalPreparationsContract.IView view;
    private IMedicalPreparationsContract.IModel model;
    private CompositeDisposable disposables = new CompositeDisposable();

    public MedicalPreparationsPresenter(IMedicalPreparationsContract.IView view, IMedicalPreparationsContract.IModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void initPresenter() {
        view.initView();
    }

    @Override
    public void loadMedicalPreparations() {
        view.loadDataIntoList();
    }

    @Override
    public void loadMedicalPreparationByTitle(String title) {
        Flowable<ResponseModel> medicalPreparations = model.getMedicalPreparationByTitle(title);
        getMedicalPreparations(medicalPreparations, false);
    }

    @Override
    public List<MedicalPreparation> loadMedicalPreparationsForSuggestions() {
        return model.getMedicalPreparationsForSuggestions();
    }

    private void getMedicalPreparations(Flowable<ResponseModel> medicalPreparations, final boolean getAllMedicalPreparations) {

        disposables.add(
                medicalPreparations.map(new Function<ResponseModel, List<MedicalPreparation>>() {
                    @Override
                    public List<MedicalPreparation> apply(ResponseModel responseModel) {

                        List<MedicalPreparation> medicalPreparationList = new ArrayList<>();

                        if (responseModel != null) {
                            if (responseModel.getResults() != null) {

                                MapperMedicalPreparations mapperMedicalPreparations = new MapperMedicalPreparations();

                                for (int i = 0; i < responseModel.getResults().size(); i++) {
                                    MedicalPreparation medicalPreparation = mapperMedicalPreparations.map(responseModel.getResults().get(i));
                                    medicalPreparationList.add(medicalPreparation);
                                }
                            }
                        }

                        return medicalPreparationList;
                    }
                })

                        .filter(new Predicate<List<MedicalPreparation>>() {
                            @Override
                            public boolean test(List<MedicalPreparation> medicalPreparationList) throws Exception {

                                boolean isMedicalPreparationListHaveValues = ((medicalPreparationList != null) && (!medicalPreparationList.isEmpty()));

                                if (!isMedicalPreparationListHaveValues) {
//                            Log.i(App.getInstance().getString(R.string.TAG_get_Preparations_List),
//                                    App.getInstance().getString(R.string.list_med_preparation_not_values));

                                    view.showError(App.getInstance().getString(R.string.list_med_preparation_not_values));
                                }

                                return isMedicalPreparationListHaveValues;
                            }
                        })

                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())

                        .subscribe(new Consumer<List<MedicalPreparation>>() {
                            @Override
                            public void accept(List<MedicalPreparation> medicalPreparationList) throws Exception {

                                if (getAllMedicalPreparations) {
                                    Helper.arrayListToJson(medicalPreparationList);
                                }

                                view.loadDataIntoList(medicalPreparationList);
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
//                        Log.e(App.getInstance().getString(R.string.TAG_get_Preparations_List),
//                                throwable.getMessage(), throwable);
                                view.showError(App.getInstance().getString(R.string.something_went_wrong));
                            }
                        }));
    }

    @Override
    public void unSubscribe() {
        disposables.clear();
    }

    @Override
    public void detachView() {
        view = null;
    }
}
