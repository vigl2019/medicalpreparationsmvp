package com.example.medicalpreparationsmvp.contract;

import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.ResponseModel;
import com.example.medicalpreparationsmvp.view.ILoadingView;

import java.util.List;

import io.reactivex.Flowable;

public interface IMedicalPreparationsContract {

    interface IView extends ILoadingView {
        void initView();
        void loadDataIntoList();
        void loadDataIntoList(List<MedicalPreparation> medicalPreparations);
        void showError(String message);
    }

    interface IPresenter {
        void initPresenter();
        void loadMedicalPreparations();
        void loadMedicalPreparationByTitle(String title);
        List<MedicalPreparation> loadMedicalPreparationsForSuggestions();
        void unSubscribe();
        void detachView();
    }

    interface IModel {
        void deleteDataBase(String dataBaseName);
        Flowable<ResponseModel> getMedicalPreparations();
        Flowable<ResponseModel> getMedicalPreparationsByPageNumber(String pageNumber);
        Flowable<ResponseModel> getMedicalPreparationByTitle(String title);
        List<MedicalPreparation> getMedicalPreparationsForSuggestions();
    }
}
