package com.example.medicalpreparationsmvp.contract;

import com.example.medicalpreparationsmvp.model.MedicalPreparation;
import com.example.medicalpreparationsmvp.model.ResponseModelId;
import com.example.medicalpreparationsmvp.view.ILoadingView;

import io.reactivex.Flowable;

public interface IMedicalPreparationContract {

    interface IView extends ILoadingView {
        void initView();
        void initActionBar();
        void loadData(MedicalPreparation medicalPreparation);
        void showError(String message);
    }

    interface IPresenter {
        void initPresenter();
        void loadMedicalPreparationById(String id);
        void unSubscribe();
        void detachView();
    }

    interface IModel {
        Flowable<ResponseModelId> getMedicalPreparationById(String id);
    }
}
